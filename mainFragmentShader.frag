#version 140

struct Material {      // structure that describes currently used material
  vec3  ambient;       // ambient component
  vec3  diffuse;       // diffuse component
  vec3  specular;      // specular component
  float shininess;     // sharpness of specular reflection

  bool  useTexture;    // defines whether the texture is used or not
};

struct Light {         // structure describing light parameters
  vec3  ambient;       // intensity & color of the ambient component
  vec3  diffuse;       // intensity & color of the diffuse component
  vec3  specular;      // intensity & color of the specular component
  vec3  position;      // light position
  vec3  spotDirection; // spotlight direction
  float spotCosCutOff; // cosine of the spotlight's half angle
  float spotExponent;  // distribution of the light energy within the reflector's cone (center->cone's edge)
  vec3 attenuation;
};

uniform sampler2D texSampler;  // sampler for the texture access

uniform Material material;
// lamp uniform
uniform bool lampLightVisible;
uniform vec3 lampPosition;
//fire uniform
uniform bool fireVisible;
uniform vec3 firePosition;
// flashlight uniform
uniform bool flashlightVisible;
uniform vec3 flashlightPosition;
uniform vec3 flashlightDirection;
uniform float flashlightIntensity;
//fog uniform
uniform bool fogVisible;

uniform float time;

uniform mat4 Vmatrix;
uniform mat4 Mmatrix;
uniform mat4 normalMatrix;

smooth in vec4 color_v;
smooth in vec2 texCoord_v;
smooth in vec3 vertexPosition;
smooth in vec3 vertexNormal;
out vec4 color_f;

//sun light = directional light
vec4 sunLight (Light light, Material material) {
	vec3 L = normalize(light.position);
	vec3 R = reflect(-L, vertexNormal);
	vec3 V = normalize(-vertexPosition);
	    
	vec3 ambient = light.ambient * material.ambient;
	vec3 diffuse = max(dot(L, vertexNormal), 0) * material.diffuse * light.diffuse;
	vec3 specular = pow(max(dot(R, V), 0), material.shininess) * material.specular * light.specular;

	return vec4(ambient + diffuse + specular, 1.0f);
}

// point light, use for lamp and fire light
vec4 pointLight(Light light, Material material) {
	vec3 lightPosition = (Vmatrix * vec4(light.position, 1.0f)).xyz;

	vec3 L = normalize(lightPosition - vertexPosition);
	vec3 R = reflect(-L, vertexNormal);
	vec3 V = normalize(-vertexPosition);

	vec3 ambient = light.ambient * material.ambient;
	vec3 diffuse = max(dot(L, vertexNormal), 0) * material.diffuse * light.diffuse;
	vec3 specular = pow(max(dot(R, V), 0), material.shininess) * material.specular * light.specular;
	

	float dist = distance(vertexPosition, lightPosition);
	float attenuation = 1.0f / (light.attenuation.x + light.attenuation.y * dist + light.attenuation.z * dist * dist);
	ambient  *= attenuation;
    diffuse  *= attenuation;
    specular *= attenuation;
    return vec4(ambient + diffuse + specular, 1.0f);
}
//flashlight light
vec4 spotLight(Light light, Material material) {
	vec3 spotDirection = normalize((Vmatrix * vec4(light.spotDirection, 0.0)).xyz);

	vec3 L = normalize(light.position - vertexPosition);
	vec3 R = reflect(-L, vertexNormal);
	vec3 V = normalize(-vertexPosition);

	vec3 ambient = light.ambient * material.ambient;
	vec3 diffuse = max(dot(L, vertexNormal), 0) * material.diffuse * light.diffuse;
	vec3 specular = pow(max(dot(R, V), 0), material.shininess) * material.specular * light.specular;

	vec3 ret = vec3(0.0);
	float cutOff = max(0.0, dot(-L, spotDirection));

	if (cutOff >= light.spotCosCutOff) {
		ret = diffuse + specular + ambient;
		ret *= pow(cutOff, light.spotExponent);
	}
	return vec4(ret, 1.0);
}

Light sun;
Light lampLight;
Light fireLight;
Light flashlightLight;

void setupLights () {
	if(fogVisible || flashlightVisible)
	{
		//sun light setup if fog or reflector turn on
		sun.ambient = vec3(0.1f, 0.3f, 0.1f);
		sun.diffuse = vec3(0.1f, 0.1f, 0.1f);
		sun.specular = vec3(0.05);
		sun.position = vec3(0.0, 0.0, 20.0);
	}
	else{
		//sun light setup 
		sun.ambient = vec3(0.15f, 0.2f, 0.2f);
		sun.diffuse = vec3(0.6f, 0.4f, 0.1f);
		sun.specular = vec3(0.05);
		sun.position = vec3(0.0, 0.0, 20.0);
	}
	

	// lamp light setup
	lampLight.ambient =  vec3(0.9f * sin(time), 0.9f * cos(time), 0.1f);
	lampLight.diffuse =  vec3(0.8f * sin(time), 0.7f * cos(time), 0.2f);
	lampLight.specular = vec3(0.05f);
	lampLight.position = lampPosition;
	lampLight.attenuation = vec3(0.99f);

	// fire light setup
	fireLight.ambient =  vec3(0.99f, 0.3f, 0.1f);
	fireLight.diffuse =  vec3(0.9f, 0.6f, 0.1f);
	fireLight.specular = vec3(0.09f);
	fireLight.position = firePosition;
	fireLight.attenuation = vec3(0.7f);

	//reflector light setup
	flashlightLight.ambient = vec3(2.0f) * flashlightIntensity;
	flashlightLight.diffuse = vec3(4.0f) * flashlightIntensity;
	flashlightLight.specular = vec3(4.0f) * flashlightIntensity;
	flashlightLight.spotCosCutOff = 0.1f;
	flashlightLight.spotExponent = 20.0f;
	flashlightLight.position = (Vmatrix * vec4(flashlightPosition, 1.0)).xyz;
	flashlightLight.spotDirection = flashlightDirection;
}

vec4 fogFunc(vec4 outputColor){
	float fogDensity = 0.1f;
	vec4 fogColor = vec4(0.55f, 0.65f, 0.6f, 0.0f);
	float fogDist = length(vertexPosition);
	float fogFactor = clamp(exp(-fogDist * fogDist * fogDensity*fogDensity), 0.1, 1.0);
	outputColor = outputColor * fogFactor + (1 - fogFactor) * fogColor;
	return outputColor;

}


void main () {
	setupLights();
	// basic output color
	vec3 worldLight = vec3(0.3f);
	vec4 outputColor = vec4(material.ambient * worldLight, 0.0);
	// apply sun light
	outputColor += sunLight(sun, material);		
	//apply lamp light if it is turn on
	if(lampLightVisible)
		outputColor += pointLight(lampLight, material);
	//apply fire light if it is turn on
	if(fireVisible)
		outputColor += pointLight(fireLight, material);
	//apply flashlight if it is turn on
	if(flashlightVisible)
		outputColor += spotLight(flashlightLight, material);
	// if material has a texture -> apply it
	if (material.useTexture)
		color_f = outputColor * texture(texSampler, texCoord_v);
	else
		color_f = outputColor;
	if(fogVisible)
		color_f = fogFunc(color_f);

	
}
