//----------------------------------------------------------------------------------------
/**
 * \file    data.h
 * \author  Hanna Korniushyna
 * \date    2022
 * \brief   Basic defines and data structures.
 */
 //----------------------------------------------------------------------------------------
#pragma once

#define SCENE_WIDTH  1.0f
#define SCENE_HEIGHT 1.0f
#define SCENE_DEPTH  1.0f
#define RENDER_DISTANCE 500.0f

enum { KEY_LEFT_ARROW, KEY_RIGHT_ARROW, KEY_UP_ARROW, KEY_DOWN_ARROW, KEY_SHIFT, KEYS_COUNT };

#define CAMERA_ELEVATION_MAX 90.0f
#define MOUSE_SENSITIVITY 0.1f


#define FIELD_OF_VIEW_1 50.0f

#define FIELD_OF_VIEW_FREE 60.0f

#define PLAYER_SPEED 0.3f

#define ID_GEM 99
#define ID_CAMPFIRE 98
#define ID_LAMP 97

#define BANNER_SIZE 1.0f
#define FIRE_SIZE 1.0f

#define GEM_DIFFRENCE 10.0f

#define FLASHLIGHT_INTENSITY 0.2f

#define LAMP_POSITION glm::vec3( -20.9f, 3.7f, 1.4f);
#define FIRE_POSITION glm::vec3(-9.95f, -16.1f, 0.6f);

#define DEFAULT_SIZE 1.0f

//
// banner geometry definition 
//

const int bannerNumQuadVertices = 4;
const float bannerVertexData[bannerNumQuadVertices * 5] = {

    // x      y      z     u     v
    -0.7f,  1.0f, 0.0f, 0.0f, 5.0f,
    -0.7f, -1.0f, 0.0f, 0.0f, 0.0f,
     0.7f,  1.0f, 0.0f, 1.0f, 5.0f,
     0.7f, -1.0f, 0.0f, 1.0f, 0.0f
};
//
// fire animation geometry definition 
//
const int fireNumQuadVertices = 4;
const float fireVertexData[fireNumQuadVertices * 5] = {
    -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
     1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
    -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
     1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
};


// default shaders - color per vertex and matrix multiplication
const std::string colorVertexShaderSrc(
    "#version 140\n"
    "uniform mat4 PVMmatrix;\n"
    "in vec3 position;\n"
    "in vec3 color;\n"
    "smooth out vec4 theColor;\n"
    "void main() {\n"
    "  gl_Position = PVMmatrix * vec4(position, 1.0);\n"
    "  theColor = vec4(color, 1.0);\n"
    "}\n"
);

const std::string colorFragmentShaderSrc(
    "#version 140\n"
    "smooth in vec4 theColor;\n"
    "out vec4 outputColor;\n"
    "void main() {\n"
    "  outputColor = theColor;\n"
    "}\n"
);