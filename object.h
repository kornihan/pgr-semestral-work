#pragma once

#include "data.h"

/**
 * \brief Shader program related stuff (id, locations, ...).
 */
typedef struct _ShaderProgram {
    // identifier for the shader program
    GLuint program;          // = 0;

    struct {
        // vertex attributes locations
        GLint pos;       // = -1;
        GLint color;	// = -1;
        GLint normal;    // = -1;
        GLint texCoord;  // = -1;
        // uniforms locations
        GLint PVMmatrix; // = -1;
        GLint Vmatrix;      // = -1;  view/camera matrix
        GLint Mmatrix;      // = -1;  modeling matrix
        GLint normalMatrix; // = -1;  inverse transposed Mmatrix
        //elapsed time in seconds
        GLint time;    // = -1;
        // material 
        GLint diffuse;    // = -1;
        GLint ambient;    // = -1;
        GLint specular;   // = -1;
        GLint shininess;  // = -1;
        // texture
        GLint useTexture; // = -1; 
        GLint texSampler; // = -1;
        //lamp
        GLint lamp;
        GLint lampPosition;
        //fire 
        GLint fire;
        GLint firePosition;
        //flashlight
        GLint flashlight;
        GLint flashlightPosition;
        GLint flashlightDirection;
        GLint flashlightIntensity;
        //fog
        GLint fog;
    } locations;
} ShaderProgram;

/**
 * \brief Geometry of an object (vertices, triangles).
 */
typedef struct _ObjectGeometry {
    GLuint        vertexBufferObject;   // identifier for the vertex buffer object
    GLuint        elementBufferObject;  // identifier for the element buffer object
    GLuint        vertexArrayObject;    // identifier for the vertex array object
    unsigned int  numTriangles;         // number of triangles in the mesh
    // material
    glm::vec3     ambient;
    glm::vec3     diffuse;
    glm::vec3     specular;
    float         shininess;
    GLuint        texture;
} ObjectGeometry;

class ObjectInstance {

protected:
    ObjectGeometry* geometry = NULL;

public:
    glm::vec3 position;
    glm::vec3 direction;
    float size;
    float startTime;
    float currentTime;
    ObjectInstance() {}
    ~ObjectInstance() {
        glDeleteVertexArrays(1, &(geometry->vertexArrayObject));
        glDeleteBuffers(1, &(geometry->elementBufferObject));
        glDeleteBuffers(1, &(geometry->vertexBufferObject));

        if (geometry->texture != 0)
            glDeleteTextures(1, &(geometry->texture));
    }
    virtual void draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {}

    virtual void startSettings() {
        size = DEFAULT_SIZE;
        position = glm::vec3(0.0f);
        direction = glm::normalize(glm::vec3(0.0f, 1.0f, 0.0f));
    }
};

typedef struct PlayerObject: public ObjectInstance{
    float   viewAngle;
    float   elevationAngle;
    float   speed;
    virtual void startSettings() override {
        size = DEFAULT_SIZE;
        position = glm::vec3(-10.0f, -5.0f, 2.0f);
        elevationAngle = 0.0f;
        viewAngle = -110.0f; // degrees
        direction = glm::vec3(sin(glm::radians(viewAngle)), cos(glm::radians(viewAngle)), 0.0f);
        speed = PLAYER_SPEED;
    }
}player;

typedef struct CorgiObject : public ObjectInstance {
    CorgiObject(const char* TEXTURE_FILE);
    virtual void draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) override;
    virtual void startSettings() override {
        size = DEFAULT_SIZE;
        position = glm::vec3(-7.0f, -18.0f, 0.3f);
    }
}corgi;

typedef struct DuckObject : public ObjectInstance {
    float viewAngle;
    DuckObject(const char* GEOMETRY_FILE, const char* TEXTURE_FILE);
    virtual void draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) override;
    virtual void startSettings() override {
        size = DEFAULT_SIZE;
        position = glm::vec3(-43.0f, -17.0f, -2.5f);
        direction = glm::vec3(0.0f);
        viewAngle = 90.0f;
    }
}duck;

typedef struct BaseObject : public ObjectInstance {
    BaseObject(const char* GEOMETRY_FILE, const char* TEXTURE_FILE);
    virtual void draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) override;
}base;

typedef struct BannerObject : public ObjectInstance {
    BannerObject(const char* TEXTURE_FILE);
    virtual void draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) override;
    virtual void startSettings() override {
        size = BANNER_SIZE;
        position = glm::vec3(0.0f);
        direction = glm::normalize(glm::vec3(0.0f, 1.0f, 0.0f));
    }
}banner;

typedef struct FireObject : public ObjectInstance {
    int    textureFrames;
    float  frameDuration;
    float   speed;
    FireObject(const char* TEXTURE_FILE);
    virtual void draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) override;
    virtual void startSettings() override {
        speed = 0.0f;
        size = FIRE_SIZE;
        direction = glm::vec3(0.0f, 1.0f, 0.0f);
        position = glm::vec3(-10.0f, -16.5f, 1.0f);
        frameDuration = 0.1f;
        textureFrames = 12;
    }
}fire;

typedef struct SkyboxObject : public ObjectInstance {
    SkyboxObject(const char* SKYBOX_CUBE_TEXTURE_FILE_PREFIX);
    void draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, bool fogVisible);
}skybox;


void initializeShaderPrograms();
void cleanupShaderPrograms();

