#version 140

//uniform sampler2D texSampler;

//struct Material {       // structure that describes currently used material
  //  vec3 ambient;       // ambient component
  //  vec3 diffuse;       // diffuse component
  //  vec3 specular;      // specular component
   // float shininess;    // sharpness of specular reflection
   // bool useTexture;    // defines whether the texture is used or not
//};

//uniform Material material;

in vec3 position;
in vec3 normal;
in vec2 texCoord;

uniform mat4 PVMmatrix;
uniform mat4 Vmatrix;
uniform mat4 Mmatrix;
uniform mat4 normalMatrix;

smooth out vec2 texCoord_v;
smooth out vec3 vertexPosition;
smooth out vec3 vertexNormal;

void main() {
    vertexPosition = (Vmatrix * Mmatrix * vec4(position, 1.0)).xyz;
    vertexNormal = normalize((Vmatrix * Mmatrix * vec4(normal, 0.0)).xyz);
    texCoord_v = texCoord;
    gl_Position = PVMmatrix * vec4(position, 1);
}