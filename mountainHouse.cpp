//----------------------------------------------------------------------------------------
/**
 * \file    mountainHouse.cpp : This file contains the 'main' function. Program execution begins and ends there.
 * \author  Hanna Korniushyna
 * \date    2022
 * \brief   Computer graphics course on FIT CTU CZ
 *
 * This is the program for PGR course of an OpenGL application written in FreeGLUT and PGR framework.
 * The theme is scene of house in the mountains. User can interact with some objects, move in the scene etc.
 * Models were downloaded from the internet, some of theme were made by me and some of theme were downloaded from models library from official 
 * PGR course website. This code were inspirated by asteroids game which we did on lessons and other semestral work.
 */

#include <iostream>
#include <list>
#include "pgr.h"
#include "object.h"
#include <random>

constexpr int WINDOW_WIDTH = 750;
constexpr int WINDOW_HEIGHT = 750;
constexpr char WINDOW_TITLE[] = "House in the mountains";

const char* MOUNTAIN_GEOMETRY_FILE = "Graphics/Models/mountains.obj";
const char* MOUNTAIN_TEXTURE_FILE = "Graphics/Models/mountains.png";
const char* HOUSE_GEOMETRY_FILE = "Graphics/Models/house.obj";
const char* HOUSE_TEXTURE_FILE = "Graphics/Models/house.png";
const char* TREE_GEOMETRY_FILE = "Graphics/Models/tree.obj";
const char* TREE_TEXTURE_FILE = "Graphics/Models/tree.png";
const char* FOX_GEOMETRY_FILE = "Graphics/Models/fox.obj";
const char* FOX_TEXTURE_FILE = "Graphics/Models/fox.png";
const char* STUMP_GEOMETRY_FILE = "Graphics/Models/stump.obj";
const char* STUMP_TEXTURE_FILE = "Graphics/Models/stump.png";
const char* ROCK_GEOMETRY_FILE = "Graphics/Models/rock.obj";
const char* ROCK_TEXTURE_FILE = "Graphics/Models/rock.png";
const char* DEER_GEOMETRY_FILE = "Graphics/Models/deer.obj";
const char* DEER_TEXTURE_FILE = "Graphics/Models/deer.png";
const char* LAKE_GEOMETRY_FILE = "Graphics/Models/lake.obj";
const char* LAKE_TEXTURE_FILE = "Graphics/Models/lake.png";
const char* GEM_GEOMETRY_FILE = "Graphics/Models/gem.obj";
const char* GEM_TEXTURE_FILE = "Graphics/Models/gem.jpg";
const char* LAMP_GEOMETRY_FILE = "Graphics/Models/lamp.obj";
const char* LAMP_TEXTURE_FILE = "Graphics/Models/lamp.jpg";
const char* CAMPFIRE_GEOMETRY_FILE = "Graphics/Models/campfire.obj";
const char* CAMPFIRE_TEXTURE_FILE = "Graphics/Models/campfire.png";
const char* DUCK_GEOMETRY_FILE = "Graphics/Models/duck.obj";
const char* DUCK_TEXTURE_FILE = "Graphics/Models/duck.png";
const char* CORGI_TEXTURE_FILE = "Graphics/Models/corgi.png";
const char* BANNER_TEXTURE_FILE = "Graphics/Banner/banner.png";
const char* FIRE_TEXTURE_FILE = "Graphics/Fire/fire.jpg";
const char* SKYBOX_PREFIX = "Graphics/Skybox/skybox";

extern ShaderProgram shaderProgram;

struct GameState {
    int windowWidth;    // set by reshape callback
    int windowHeight;   // set by reshape callback

    bool freeCameraMode;        // false;
    bool staticShot1;     //true
    bool staticShot2;    //false
    bool staticShot3;    //false
    bool cursorVisible;  //false

    bool bannerVisible; //false
    bool fireVisible;//false
    bool lampLightVisible; //false
    bool flashlightVisible;//false
    bool fogVisible; //false

    bool gameOver;              // false;
    bool keyMap[KEYS_COUNT];    // false

    float elapsedTime;
} gameState;

struct GameObjects {
    PlayerObject* player;
    BaseObject* mountain; //NULL
    BaseObject* house; //NULL
    BaseObject* tree; //NULL
    BaseObject* fox; //NULL
    BaseObject* stump; //NULL
    BaseObject* rock; //NULL
    BaseObject* deer; //NULL
    BaseObject* lake; //NULL
    BaseObject* gem; //NULL
    BaseObject* lamp; //NULL
    BaseObject* campfire;//NULL
    DuckObject* duck; //NULL
    CorgiObject* corgi; //NULL
    BannerObject* banner;//NULL
    FireObject* fire; //NULL
    SkyboxObject* skybox;//NULL
} gameObjects;

/**
 * \brief Generate random gem position in selected area.
 * \return generated position
 */
glm::vec3 generateRandomGemPosition() {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_real_distribution<float> posX(gameObjects.gem->position.x - GEM_DIFFRENCE, gameObjects.gem->position.x + GEM_DIFFRENCE);
    std::uniform_real_distribution<float> posY(gameObjects.gem->position.y - GEM_DIFFRENCE, gameObjects.gem->position.y + GEM_DIFFRENCE);
    glm::vec3 position = glm::vec3(posX(rng), posY(rng), gameObjects.gem->position.z);

    return position;
}
/**
 * \brief Restart game settings.
 *  Restart/inicialize all settings when game restart/start
 */
void restartGame(void) {
    gameState.elapsedTime = 0.001f * (float)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds
    gameState.freeCameraMode = false;
    gameState.staticShot1 = true;
    gameState.staticShot2 = false;
    gameState.staticShot3 = false;
    gameState.cursorVisible = false;
    gameState.bannerVisible = false;
    gameState.fireVisible = false;
    gameState.lampLightVisible = false;
    gameState.flashlightVisible = false;
    gameState.fogVisible = false;
    glutSetCursor(GLUT_CURSOR_NONE);
    glutPassiveMotionFunc(NULL);
    // initialize the scene objects and create their geometry if they don't have it
    //player
    gameObjects.player = new PlayerObject();
    gameObjects.player->startSettings();
    //mountain
    if (gameObjects.mountain == NULL) {
        gameObjects.mountain = new BaseObject(MOUNTAIN_GEOMETRY_FILE, MOUNTAIN_TEXTURE_FILE);
    }
    gameObjects.mountain->startSettings();
    //house
    if (gameObjects.house == NULL) {
        gameObjects.house = new BaseObject(HOUSE_GEOMETRY_FILE, HOUSE_TEXTURE_FILE);
    }
    gameObjects.house->startSettings();
    //tree
    if (gameObjects.tree == NULL) {
        gameObjects.tree = new BaseObject(TREE_GEOMETRY_FILE, TREE_TEXTURE_FILE);
    }
    gameObjects.tree->startSettings();
    //fox
    if (gameObjects.fox == NULL) {
        gameObjects.fox = new BaseObject(FOX_GEOMETRY_FILE, FOX_TEXTURE_FILE);
    }
    gameObjects.fox->startSettings();
    //stump
    if (gameObjects.stump == NULL) {
        gameObjects.stump = new BaseObject(STUMP_GEOMETRY_FILE, STUMP_TEXTURE_FILE);
    }
    gameObjects.stump->startSettings();
    //rock
    if (gameObjects.rock == NULL) {
        gameObjects.rock = new BaseObject(ROCK_GEOMETRY_FILE, ROCK_TEXTURE_FILE);
    }
    gameObjects.rock->startSettings();
    //deer
    if (gameObjects.deer == NULL) {
        gameObjects.deer = new BaseObject(DEER_GEOMETRY_FILE, DEER_TEXTURE_FILE);
    }
    gameObjects.deer->startSettings();
    //lake
    if (gameObjects.lake == NULL) {
        gameObjects.lake = new BaseObject(LAKE_GEOMETRY_FILE, LAKE_TEXTURE_FILE);
    }
    gameObjects.lake->startSettings();
    //gem
    if (gameObjects.gem == NULL) {
        gameObjects.gem = new BaseObject(GEM_GEOMETRY_FILE, GEM_TEXTURE_FILE);
    }
    gameObjects.gem->startSettings();
    gameObjects.gem->position = generateRandomGemPosition();
    //lamp
    if (gameObjects.lamp == NULL) {
        gameObjects.lamp = new BaseObject(LAMP_GEOMETRY_FILE, LAMP_TEXTURE_FILE);
    }
    gameObjects.lamp->startSettings();
    //campfire
    if (gameObjects.campfire == NULL) {
        gameObjects.campfire = new BaseObject(CAMPFIRE_GEOMETRY_FILE, CAMPFIRE_TEXTURE_FILE);
    }
    gameObjects.campfire->startSettings();
    // duck
    if (gameObjects.duck == NULL) {
        gameObjects.duck = new DuckObject(DUCK_GEOMETRY_FILE, DUCK_TEXTURE_FILE);
        gameObjects.duck->startSettings();
    }
   
    //gameObjects.duck->startTime = gameState.elapsedTime;
   // gameObjects.duck->currentTime = gameObjects.duck->startTime;
    //corgi
    if (gameObjects.corgi == NULL) {
        gameObjects.corgi = new CorgiObject(CORGI_TEXTURE_FILE);
    }
    gameObjects.corgi->startSettings();
    //banner
    if (gameObjects.banner == NULL) {
        gameObjects.banner = new BannerObject(BANNER_TEXTURE_FILE);
    }
    gameObjects.banner->startSettings();
    gameObjects.banner->startTime = gameState.elapsedTime;
    gameObjects.banner->currentTime = gameObjects.banner->startTime;
    //fire
    if (gameObjects.fire == NULL) {
        gameObjects.fire = new FireObject(FIRE_TEXTURE_FILE);
    }
    gameObjects.fire->startSettings();
    gameObjects.fire->startTime = gameState.elapsedTime;
    gameObjects.fire->currentTime = gameObjects.fire->startTime;
    if (gameObjects.skybox == NULL) {
        gameObjects.skybox = new SkyboxObject(SKYBOX_PREFIX);
    }
    // reset key map
    for (int i = 0; i < KEYS_COUNT; i++) {
        gameState.keyMap[i] = false;
    }

    gameState.gameOver = false;

}

/**
 * \brief GLUT menu callback.
 * \param value number of action
 */
void glutMenuCb(int value) {
    switch (value) {
    case 0:
        gameState.staticShot1 = true;
        gameState.staticShot2 = false;
        gameState.staticShot3 = false;
        gameState.freeCameraMode = false;
        glutSetCursor(GLUT_CURSOR_NONE);
        gameState.cursorVisible = false;
        glutDetachMenu(GLUT_RIGHT_BUTTON);
        glutPassiveMotionFunc(NULL);
        break;
    case 1:
        gameState.staticShot1 = false;
        gameState.staticShot2 = true;
        gameState.staticShot3 = false;
        gameState.freeCameraMode = false;
        glutSetCursor(GLUT_CURSOR_NONE);
        gameState.cursorVisible = false;
        glutDetachMenu(GLUT_RIGHT_BUTTON);
        glutPassiveMotionFunc(NULL);
        break;
    case 2:
        gameState.staticShot1 = false;
        gameState.staticShot2 = false;
        gameState.staticShot3 = true;
        gameState.freeCameraMode = false;
        glutSetCursor(GLUT_CURSOR_NONE);
        gameState.cursorVisible = false;
        glutDetachMenu(GLUT_RIGHT_BUTTON);
        glutPassiveMotionFunc(NULL);
        break;
    case 4:
        gameState.flashlightVisible = !gameState.flashlightVisible;
        break;
    case 5:
        gameState.fogVisible = !gameState.fogVisible;
        break;
    case 6:
        gameState.lampLightVisible = !gameState.lampLightVisible;
        break;
    case 7:
        gameState.fireVisible = !gameState.fireVisible;
        break;
    case 8:
        restartGame();
        break;
    case 9:
        glutLeaveMainLoop();
        break;
    default:
        ;
    }

}

/**
 * \brief Create right-click GLUT menu.
 */
void createGLUTMenu() {
    int camerasMenu, interactionMenu;

    // switching camera
    camerasMenu = glutCreateMenu(glutMenuCb);
    glutAddMenuEntry("Top camera", 0);
    glutAddMenuEntry("Static shot house", 1);
    glutAddMenuEntry("Static shot lake", 2);

    //turn on/off interactions
    interactionMenu = glutCreateMenu(glutMenuCb);
    glutAddMenuEntry("Flashlight", 4);
    glutAddMenuEntry("Fog", 5);
    glutAddMenuEntry("Lamp", 6);
    glutAddMenuEntry("Fire", 7);
    // create a menu with submenus
    glutCreateMenu(glutMenuCb);
    glutAddSubMenu("Camera", camerasMenu);
    glutAddSubMenu("Interactions", interactionMenu);
    glutAddMenuEntry("Restart game", 8);
    glutAddMenuEntry("Exit", 9);
}




/**
 * \brief Draw window content.
 * Draw cameras, make camera view and projection matrix, draw all objects in the scene etc.
 */
void drawWindowContents() {
    // setup parallel projection
    glm::mat4 orthoProjectionMatrix = glm::ortho(
        -SCENE_WIDTH, SCENE_WIDTH,
        -SCENE_HEIGHT, SCENE_HEIGHT,
        -10.0f * SCENE_DEPTH, 10.0f * SCENE_DEPTH
    );
    // static viewpoint - top view
    glm::mat4 orthoViewMatrix = glm::lookAt(
        glm::vec3(0.0f, 0.0f, 1.0f),
        glm::vec3(0.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 1.0f, 0.0f)
    );

    // setup camera & projection transform
    glm::mat4 viewMatrix = orthoViewMatrix;
    glm::mat4 projectionMatrix = orthoProjectionMatrix;
    glm::vec3 cameraViewDirection = gameObjects.player->direction;
    if (gameState.freeCameraMode == true || gameState.staticShot2 == true || gameState.staticShot3 == true) {
        if (gameState.staticShot2 == true) {
            gameObjects.player->startSettings();
            float angle = glm::radians(gameObjects.player->viewAngle);
            gameObjects.player->direction.x = cos(angle);
            gameObjects.player->direction.y = sin(angle);
        }
        else if (gameState.staticShot3 == true) {
            gameObjects.player->startSettings();
            gameObjects.player->position = glm::vec3(-63.0f, 12.0f, 2.0f);
            gameObjects.player->viewAngle = -30.0f; // degrees
            gameObjects.player->direction = glm::vec3(sin(glm::radians(gameObjects.player->viewAngle)), cos(glm::radians(gameObjects.player->viewAngle)), 0.0f);
            float angle = glm::radians(gameObjects.player->viewAngle);
            gameObjects.player->direction.x = cos(angle);
            gameObjects.player->direction.y = sin(angle);
        }
        cameraViewDirection = gameObjects.player->direction;
        glm::vec3 cameraPosition = glm::vec3(gameObjects.player->position.x, gameObjects.player->position.y, gameObjects.player->position.z);
        glm::vec3 cameraUpVector = glm::vec3(0.0f, 0.0f, 1.0f);
        glm::vec3 cameraCenter;

        glm::vec3 rotationAxis = glm::cross(cameraViewDirection, cameraUpVector);
        glm::mat4 cameraTransform = glm::rotate(glm::mat4(1.0f), glm::radians(-gameObjects.player->elevationAngle), rotationAxis);

        cameraUpVector = glm::vec3(cameraTransform * glm::vec4(cameraUpVector, 1.0f));
        cameraViewDirection = glm::vec3(cameraTransform * glm::vec4(cameraViewDirection, 1.0f));
        cameraCenter = cameraPosition + cameraViewDirection;

        viewMatrix = glm::lookAt(
            cameraPosition,
            cameraCenter,
            cameraUpVector
        );
        projectionMatrix = glm::perspective(glm::radians(FIELD_OF_VIEW_FREE), gameState.windowWidth / (float)gameState.windowHeight, 0.1f, RENDER_DISTANCE);
    }
    else if (gameState.staticShot1 == true) {
        glm::vec3 cameraPosition = glm::vec3(-100.0f, 0.0f, 30.0f);
        glm::vec3 cameraUpVector = glm::vec3(0.0f, 0.0f, 1.0f);
        glm::vec3 cameraCenter = glm::vec3(1.0f, 0.0f, 10.0f);

        viewMatrix = glm::lookAt(
            cameraPosition,
            cameraCenter,
            cameraUpVector
        );

        projectionMatrix = glm::perspective(glm::radians(FIELD_OF_VIEW_1), gameState.windowWidth / (float)gameState.windowHeight, 0.1f, RENDER_DISTANCE);
    }
    glm::vec3 lampPosition = LAMP_POSITION;
    glm::vec3 firePosition = FIRE_POSITION;
    // Set model uniforms
    glUseProgram(shaderProgram.program);
    // Set time uniform
    glUniform1f(shaderProgram.locations.time, gameState.elapsedTime);
    // set lamp uniform
    glUniform1i(shaderProgram.locations.lamp, gameState.lampLightVisible);
    glUniform3fv(shaderProgram.locations.lampPosition, 1, glm::value_ptr(lampPosition));
    //set fire uniform
    glUniform1i(shaderProgram.locations.fire, gameState.fireVisible);
    glUniform3fv(shaderProgram.locations.firePosition, 1, glm::value_ptr(firePosition));
    //set flashlight uniform
    glUniform1i(shaderProgram.locations.flashlight, gameState.flashlightVisible);
    glUniform3fv(shaderProgram.locations.flashlightPosition, 1, glm::value_ptr(gameObjects.player->position));
    glUniform3fv(shaderProgram.locations.flashlightDirection, 1, glm::value_ptr(cameraViewDirection));
    glUniform1f(shaderProgram.locations.flashlightIntensity, FLASHLIGHT_INTENSITY);
    //set fog uniform
    glUniform1i(shaderProgram.locations.fog, gameState.fogVisible);
    glUseProgram(0);

    //Stencil Clicking
    int id = 0;
    //gem
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    CHECK_GL_ERROR();

    id = ID_GEM;
    glStencilFunc(GL_ALWAYS, id, 255);
    gameObjects.gem->draw(viewMatrix, projectionMatrix);

    glDisable(GL_STENCIL_TEST);
    CHECK_GL_ERROR();
    //lamp
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    CHECK_GL_ERROR();

    id = ID_LAMP;
    glStencilFunc(GL_ALWAYS, id, 255);
    gameObjects.lamp->draw(viewMatrix, projectionMatrix);

    glDisable(GL_STENCIL_TEST);
    CHECK_GL_ERROR();
    //campfire
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    CHECK_GL_ERROR();

    id = ID_CAMPFIRE;
    glStencilFunc(GL_ALWAYS, id, 255);
    gameObjects.campfire->draw(viewMatrix, projectionMatrix);

    glDisable(GL_STENCIL_TEST);
    CHECK_GL_ERROR();
 
    gameObjects.duck->draw(viewMatrix, projectionMatrix);
    // draw base scene objects
    gameObjects.skybox->draw(viewMatrix, projectionMatrix, gameState.fogVisible);
    gameObjects.mountain->draw(viewMatrix, projectionMatrix);
    gameObjects.house->draw(viewMatrix, projectionMatrix);
    gameObjects.tree->draw(viewMatrix, projectionMatrix);
    gameObjects.fox->draw(viewMatrix, projectionMatrix);
    gameObjects.stump->draw(viewMatrix, projectionMatrix);
    gameObjects.rock->draw(viewMatrix, projectionMatrix);
    gameObjects.deer->draw(viewMatrix, projectionMatrix);
    gameObjects.lake->draw(viewMatrix, projectionMatrix);
    gameObjects.corgi->draw(viewMatrix, projectionMatrix);

    if (gameState.bannerVisible == true) {
        gameObjects.banner->draw(orthoViewMatrix, orthoProjectionMatrix);
    }
    if (gameState.fireVisible == true) {
        glDisable(GL_DEPTH_TEST);
        gameObjects.fire->draw(viewMatrix, projectionMatrix);
        glEnable(GL_DEPTH_TEST);
    }

}
/**
 * \brief Update objects.
 * Update game objects depend on time
 * \param elapsedTime elapsed time
 */
void updateObjects(float elapsedTime) {
    // Update player direction
    float angle = glm::radians(gameObjects.player->viewAngle);
    gameObjects.player->direction.x = cos(angle);
    gameObjects.player->direction.y = sin(angle);
    // update banner 
    gameObjects.banner->currentTime = elapsedTime;
    // update fire
    gameObjects.fire->currentTime = elapsedTime;
    //update duck position and rotation
    float duckAngle = glm::radians(gameObjects.duck->viewAngle);
    gameObjects.duck->viewAngle += 0.0269f;
    gameObjects.duck->direction.x = cos(duckAngle);
    gameObjects.duck->direction.y = sin(duckAngle);
    gameObjects.duck->position = glm::vec3(gameObjects.duck->position.x + cos(elapsedTime * 0.8f) / 6, gameObjects.duck->position.y + sin(elapsedTime * 0.8f) / 6, gameObjects.duck->position.z);

}
/**
 * \brief Check if player in the playing area.
 * \return true if player in the area, otherwise false
 */
bool inPlayingArea() {
   // printf("%f %f\n", gameObjects.player->position.x, gameObjects.player->position.y);
    if (gameObjects.player->position.x < 19 && gameObjects.player->position.x > -75 &&
        gameObjects.player->position.y < 22 && gameObjects.player->position.y > -36) {
        return true;
    }
    return false;
}

/**
 * \brief Check if player in collision with house.
 * \return true if player in collision, otherwise false
 */
bool inCollisionHouse() {
    float houseRadius = 4.2f;
    float playerRadius = 0.3f;
    glm::vec3 housePosition = glm::vec3(-11.63f, -25.24f, 0.0f);
    float distanceX = housePosition.x - gameObjects.player->position.x;
    float distanceY = housePosition.y - gameObjects.player->position.y;
    glm::vec3 diffVec = glm::vec3(gameObjects.player->position.x - housePosition.x, gameObjects.player->position.y - housePosition.y, gameObjects.player->position.z - housePosition.z);
    float distance = length(diffVec);
    float radius = (houseRadius + playerRadius);
    if (distance <= radius)
        return true;
    return false;
}
/**
 * \brief Move player depend on direction.
 * \param direction in which direction player should move
 */
void move(int direction) {
    float rightSide = gameObjects.player->viewAngle - 90.0f;
    float leftSide = gameObjects.player->viewAngle + 90.0f;
    glm::vec3 oldPosition = gameObjects.player->position;

    switch (direction) {
    case 0: // move right
        gameObjects.player->position += glm::vec3(cos(glm::radians(rightSide)), sin(glm::radians(rightSide)), 0.0f) * gameObjects.player->speed;
        break;
    case 1: // move left
        gameObjects.player->position += glm::vec3(cos(glm::radians(leftSide)), sin(glm::radians(leftSide)), 0.0f) * gameObjects.player->speed;
        break;
        
    case 2: // move forward
        gameObjects.player->position += gameObjects.player->direction * gameObjects.player->speed;
        break;
    case 3: // move back
        gameObjects.player->position -= gameObjects.player->direction * gameObjects.player->speed;
        break;
    }
    if (inPlayingArea() == false || inCollisionHouse() == true) {
        gameObjects.player->position = oldPosition;
    }
}

/**
 * \brief Handle mouse movement over the window (with no button pressed).
 * \param mouseX mouse (cursor) X position
 * \param mouseY mouse (cursor) Y position
 */
void passiveMouseMotionCb(int mouseX, int mouseY) {
    if (mouseY != gameState.windowHeight / 2 || mouseX != gameState.windowWidth / 2) {
        float cameraElevationAngleDeltaY = MOUSE_SENSITIVITY * (mouseY - gameState.windowHeight / 2);
        float cameraElevationAngleDeltaX = MOUSE_SENSITIVITY * (mouseX - gameState.windowWidth / 2);
        if (fabs(gameObjects.player->elevationAngle + cameraElevationAngleDeltaY) < CAMERA_ELEVATION_MAX) {
            gameObjects.player->elevationAngle += cameraElevationAngleDeltaY;
        }

        gameObjects.player->viewAngle -= cameraElevationAngleDeltaX;
        if (gameObjects.player->viewAngle > 360.0f) {
            gameObjects.player->viewAngle -= 360.0f;
        }

        if (gameObjects.player->viewAngle < -360.0f) {
            gameObjects.player->viewAngle += 360.0f;
        }
        gameObjects.player->direction = glm::vec3(cos(glm::radians(gameObjects.player->viewAngle)), sin(glm::radians(gameObjects.player->viewAngle)), 0.0f);

        glutWarpPointer(gameState.windowWidth / 2, gameState.windowHeight / 2);
        // create display event to redraw window contents
        glutPostRedisplay();
    }
}

// -----------------------  Window callbacks ---------------------------------

/**
 * \brief Draw the window contents.
 * Called to update the display. You should call glutSwapBuffers after all of your
 * rendering to display what you rendered.
 */
void displayCb() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // draw the window contents (scene objects) 
    drawWindowContents();
    glutSwapBuffers();
}

/**
 * \brief Window was reshaped.
 * \param newWidth New window width
 * \param newHeight New window height
 */
void reshapeCb(int newWidth, int newHeight) {
    gameState.windowWidth = newWidth;
    gameState.windowHeight = newHeight;
    glViewport(0, 0, (GLsizei)newWidth, (GLsizei)newHeight);
}


// -----------------------  Keyboard ---------------------------------
/**
 * \brief Handle the key pressed event.
 * Called whenever a key on the keyboard was pressed. The key is given by the "keyPressed"
 * parameter, which is an ASCII character. It's often a good idea to have the escape key (ASCII value 27)
 * to call glutLeaveMainLoop() to exit the program.
 * \param keyPressed ASCII code of the key
 * \param mouseX mouse (cursor) X position
 * \param mouseY mouse (cursor) Y position
 */
void keyboardCb(unsigned char keyPressed, int mouseX, int mouseY) {
    switch (keyPressed) {
        case 27: // escape
            glutLeaveMainLoop();
            break;
        case 'R': // restart game
            if(gameState.keyMap[KEY_SHIFT] == true)
                restartGame();
            break;
        case 'r':
            if (gameState.freeCameraMode == true) 
                gameState.flashlightVisible = !gameState.flashlightVisible;
            break;
        case 'f':
            if(!gameState.staticShot1)
                gameState.fogVisible = !gameState.fogVisible;
            break;
        case 'w': //move forward
            gameState.keyMap[KEY_UP_ARROW] = true;
            break;
        case 'a': //move left
            gameState.keyMap[KEY_LEFT_ARROW] = true;
            break;
        case 's': //move back
            gameState.keyMap[KEY_DOWN_ARROW] = true;
            break;
        case 'd': //move right
            gameState.keyMap[KEY_RIGHT_ARROW] = true;
            break;
        case 'c': //camera free mode
            if (gameState.cursorVisible == true) {
                glutSetCursor(GLUT_CURSOR_NONE);
                gameState.cursorVisible = false;
                glutDetachMenu(GLUT_RIGHT_BUTTON);
            }
            gameState.staticShot1 = false;
            gameState.staticShot2 = false;
            gameState.staticShot3 = false;
            gameState.freeCameraMode = true;
            glutPassiveMotionFunc(passiveMouseMotionCb);
            break;
        case 'm':
            if (gameState.cursorVisible == true) {
                glutSetCursor(GLUT_CURSOR_NONE);
                glutPassiveMotionFunc(passiveMouseMotionCb);
                gameState.cursorVisible = false;
                glutDetachMenu(GLUT_RIGHT_BUTTON);
            }
            else if (gameState.freeCameraMode == true) {
                glutPassiveMotionFunc(NULL);
                glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
                gameState.cursorVisible = true;
                createGLUTMenu();
                // open menu on right click
                glutAttachMenu(GLUT_RIGHT_BUTTON);
            }
            break;
        default:
           ;
    }
}

// Called whenever a key on the keyboard was released. The key is given by
// the "keyReleased" parameter, which is in ASCII. 
/**
 * \brief Handle the key released event.
 * \param keyReleased ASCII code of the released key
 * \param mouseX mouse (cursor) X position
 * \param mouseY mouse (cursor) Y position
 */
void keyboardUpCb(unsigned char keyReleased, int mouseX, int mouseY) {
    switch (keyReleased) {
        case 'w': //stop move forward
            gameState.keyMap[KEY_UP_ARROW] = false;
            break;
        case 'a': //stop move left
            gameState.keyMap[KEY_LEFT_ARROW] = false;
            break;
        case 's': //stop move back
            gameState.keyMap[KEY_DOWN_ARROW] = false;
            break;
        case 'd': //stop move right
            gameState.keyMap[KEY_RIGHT_ARROW] = false;
            break;
        default:
            ;// printf("Unrecognized key released\n");
    }
}

/**
 * \brief Handle the non-ASCII key pressed event (such as arrows of F1).
 *  The special keyboard callback is triggered when keyboard function (Fx) or directional
 *  keys are pressed.
 * \param specKeyPressed int value of a predefined glut constant such as GLUT_KEY_UP
 * \param mouseX mouse (cursor) X position
 * \param mouseY mouse (cursor) Y position
 */
void specialKeyboardCb(int specKeyPressed, int mouseX, int mouseY) {
    if (gameState.gameOver == true)
        return;

    switch (specKeyPressed) {
    case GLUT_KEY_RIGHT:
        gameState.keyMap[KEY_RIGHT_ARROW] = true;
        break;
    case GLUT_KEY_LEFT:
        gameState.keyMap[KEY_LEFT_ARROW] = true;
        break;
    case GLUT_KEY_UP:
        gameState.keyMap[KEY_UP_ARROW] = true;
        break;
    case GLUT_KEY_DOWN:
        gameState.keyMap[KEY_DOWN_ARROW] = true;
        break;
    case GLUT_KEY_F1:
        gameState.staticShot1 = true;
        gameState.staticShot2 = false;
        gameState.staticShot3 = false;
        gameState.freeCameraMode = false;
        glutPassiveMotionFunc(NULL);
        gameState.cursorVisible = false;
        glutDetachMenu(GLUT_RIGHT_BUTTON);
        break;
    case GLUT_KEY_F2:
        gameState.staticShot1 = false;
        gameState.staticShot2 = true;
        gameState.staticShot3 = false;
        gameState.freeCameraMode = false;
        glutPassiveMotionFunc(NULL);
        gameState.cursorVisible = false;
        glutDetachMenu(GLUT_RIGHT_BUTTON);
        break;
    case GLUT_KEY_F3:
        gameState.staticShot1 = false;
        gameState.staticShot2 = false;
        gameState.staticShot3 = true;
        gameState.freeCameraMode = false;
        glutPassiveMotionFunc(NULL);
        gameState.cursorVisible = false;
        glutDetachMenu(GLUT_RIGHT_BUTTON);
        break;
    case GLUT_KEY_SHIFT_L:
        gameState.keyMap[KEY_SHIFT] = true;
        break;
    case GLUT_KEY_SHIFT_R:
        gameState.keyMap[KEY_SHIFT] = true;
        break;
    default:
        ; // printf("Unrecognized special key pressed\n");
    }
}
/**
 * \brief Handle the non-ASCII key released event (such as arrows of F1).
 *  The special keyboard callback is triggered when keyboard function (Fx) or directional
 *  keys are released.
 * \param specKeyReleased int value of a predefined glut constant such as GLUT_KEY_UP
 * \param mouseX mouse (cursor) X position
 * \param mouseY mouse (cursor) Y position
 */
void specialKeyboardUpCb(int specKeyReleased, int mouseX, int mouseY) {
    if (gameState.gameOver == true)
        return;

    switch (specKeyReleased) {
    case GLUT_KEY_RIGHT:
        gameState.keyMap[KEY_RIGHT_ARROW] = false;
        break;
    case GLUT_KEY_LEFT:
        gameState.keyMap[KEY_LEFT_ARROW] = false;
        break;
    case GLUT_KEY_UP:
        gameState.keyMap[KEY_UP_ARROW] = false;
        break;
    case GLUT_KEY_DOWN:
        gameState.keyMap[KEY_DOWN_ARROW] = false;
        break;
    case GLUT_KEY_CTRL_L:
        gameState.keyMap[KEY_SHIFT] = false;
        break;
    case GLUT_KEY_CTRL_R:
        gameState.keyMap[KEY_SHIFT] = false;
        break;
    default:
        ; // printf("Unrecognized special key released\n");
    }
} 

// -----------------------  Mouse ---------------------------------
// three events - mouse click, mouse drag, and mouse move with no button pressed

// 
/**
 * \brief React to mouse button press and release (mouse click).
 * When the user presses and releases mouse buttons in the window, each press
 * and each release generates a mouse callback (including release after dragging).
 *
 * \param buttonPressed button code (GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, or GLUT_RIGHT_BUTTON)
 * \param buttonState GLUT_DOWN when pressed, GLUT_UP when released
 * \param mouseX mouse (cursor) X position
 * \param mouseY mouse (cursor) Y position
 */
void mouseCb(int buttonPressed, int buttonState, int mouseX, int mouseY) {
    unsigned char id = 0;
    // do picking only on mouse down
    if ((buttonPressed == GLUT_LEFT_BUTTON) && (buttonState == GLUT_DOWN)) {
        glReadPixels(mouseX, gameState.windowHeight - mouseY, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &id);

        glDisable(GL_STENCIL_TEST);

        if (id == 0) {
            // background was clicked
            printf("Clicked on background\n");
        }
        else {
            // object was clicked
            printf("Clicked on object with ID: %d\n", (int)id);
            if (id == ID_GEM) {
                gameObjects.banner->startTime = gameState.elapsedTime;
                gameState.bannerVisible = true;
            }
            else if (id == ID_CAMPFIRE) {
                gameState.fireVisible = !gameState.fireVisible;
            }
            else if (id == ID_LAMP) {
                gameState.lampLightVisible = !gameState.lampLightVisible;
            }
      
        }
    }

    id = 0;
}

// -----------------------  Timer ---------------------------------

/**
 * \brief Callback responsible for the scene update.
 */
void timerCb(int)
{
    // update scene times
    gameState.elapsedTime = 0.001f * (float)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds
    // call appropriate actions according to the currently pressed keys in key map
    // (combinations of keys are supported but not used in this implementation)
    if (gameState.keyMap[KEY_RIGHT_ARROW] == true)
        move(0); // move right
    else if (gameState.keyMap[KEY_LEFT_ARROW] == true)
        move(1); // move left
    if (gameState.keyMap[KEY_UP_ARROW] == true)
        move(2); // move forward
    else if (gameState.keyMap[KEY_DOWN_ARROW] == true)
        move(3); // move back
    
    // update the application state
    updateObjects(gameState.elapsedTime);
    // and plan a new event
    
    // set timeCallback next invocation
    glutTimerFunc(33, timerCb, 0);
    // create display event
    glutPostRedisplay();
}
// -----------------------  Application ---------------------------------

/**
 * \brief Initialize application data and OpenGL stuff.
 */
void initApplication() {
    // initialize random seed
    srand((unsigned int)time(NULL));

    // initialize OpenGL
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glEnable(GL_DEPTH_TEST);

    // initialize shaders
    initializeShaderPrograms(); 
    gameObjects.mountain = NULL;
    gameObjects.house = NULL;
    gameObjects.tree = NULL;
    gameObjects.fox = NULL;
    gameObjects.stump = NULL;
    gameObjects.rock = NULL;
    gameObjects.deer = NULL;
    gameObjects.lake = NULL;
    gameObjects.gem = NULL;
    gameObjects.lamp = NULL;
    gameObjects.campfire = NULL;
    gameObjects.duck = NULL;
    gameObjects.corgi = NULL;
    gameObjects.banner = NULL;
    gameObjects.fire = NULL;
    gameObjects.skybox = NULL;
   // initializeModels();
    restartGame();
}
/**
 * \brief Delete all OpenGL objects and application data.
 */
void finalizeApplication(void) {

    // delete buffers

    delete gameObjects.mountain;
    gameObjects.mountain = NULL;

    delete gameObjects.house;
    gameObjects.house = NULL;

    delete gameObjects.tree;
    gameObjects.tree = NULL;

    delete gameObjects.fox;
    gameObjects.fox = NULL;

    delete gameObjects.stump;
    gameObjects.stump = NULL;

    delete gameObjects.rock;
    gameObjects.rock = NULL;

    delete gameObjects.deer;
    gameObjects.deer = NULL;

    delete gameObjects.lake;
    gameObjects.lake = NULL;
   
    delete gameObjects.gem;
    gameObjects.gem = NULL;

    delete gameObjects.lamp;
    gameObjects.lamp = NULL;

    delete gameObjects.campfire;
    gameObjects.campfire = NULL;

    delete gameObjects.duck;
    gameObjects.duck = NULL;

    delete gameObjects.corgi;
    gameObjects.corgi = NULL;

    delete gameObjects.banner;
    gameObjects.banner = NULL;

    delete gameObjects.fire;
    gameObjects.fire = NULL;

    delete gameObjects.skybox;
    gameObjects.skybox = NULL;
    // delete shaders
    cleanupShaderPrograms();
}

/**
 * \brief Entry point of the application.
 * \param argc number of command line arguments
 * \param argv array with argument strings
 * \return 0 if OK, <> elsewhere
 */
int main(int argc, char** argv) {
    srand(static_cast <unsigned> (time(0)));

    // initialize the GLUT library (windowing system)
    glutInit(&argc, argv);

    glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
    // for each window
    {
        //   initial window size + title
        glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        glutCreateWindow(WINDOW_TITLE);

        // Callbacks - use only those you need
        glutDisplayFunc(displayCb);
        glutReshapeFunc(reshapeCb);
        glutKeyboardFunc(keyboardCb);
        glutKeyboardUpFunc(keyboardUpCb);
        glutSpecialFunc(specialKeyboardCb);     // key pressed
        glutSpecialUpFunc(specialKeyboardUpCb); // key released
        glutMouseFunc(mouseCb);
        //glutMotionFunc(mouseMotionCb);


        glutTimerFunc(33, timerCb, 0);
    }
    // end for each window 

    // initialize PGR framework (GL, DevIl, etc.)
    if (!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR)) {
        pgr::dieWithError("pgr init failed, required OpenGL not supported?");
    }
    // init your stuff - shaders & program, buffers, locations, state of the application
    initApplication();

    // handle window close by the user
    glutCloseFunc(finalizeApplication);

    // Infinite loop handling the events
    glutMainLoop();

    // code after glutLeaveMainLoop()
    // cleanup

    return 0;
}