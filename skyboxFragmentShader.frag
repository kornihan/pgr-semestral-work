#version 140

// fragment shader uses interpolated 3D tex coords to sample cube map
uniform samplerCube skyboxSampler;
//fog uniform
uniform bool fogVisible;

in vec3 texCoord_v;
out vec4 color_f;

vec4 fogFunc(vec4 outputColor){
	float fogDensity = 0.1f;
	vec4 fogColor = vec4(0.55f, 0.65f, 0.6f, 0.0f);
	float fogDist = 20.0f;
	float fogFactor = clamp(exp(-fogDist * fogDist * fogDensity*fogDensity), 0.1, 1.0);
	outputColor = outputColor * fogFactor + (1 - fogFactor) * fogColor;
	return outputColor;

}
void main() {
	color_f = texture(skyboxSampler, texCoord_v);
	if(fogVisible)
		color_f = fogFunc(color_f);
}