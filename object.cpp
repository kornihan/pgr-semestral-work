#include <iostream>
#include "pgr.h"
#include "object.h"
#include "corgi.h"

ShaderProgram shaderProgram;

struct fireShaderProgram {
    // identifier for the program
    GLuint program;              // = 0;
    // vertex attributes locations
    GLint posLocation;           // = -1;
    GLint texCoordLocation;      // = -1;
    // uniforms locations
    GLint PVMmatrixLocation;     // = -1;
    GLint VmatrixLocation;       // = -1;
    GLint timeLocation;          // = -1;
    GLint texSamplerLocation;    // = -1;
    GLint frameDurationLocation; // = -1;
} fireShaderProgram;

struct bannerShaderProgram {
    // identifier for the program
    GLuint program;           // = 0;
    // vertex attributes locations
    GLint posLocation;        // = -1;
    GLint texCoordLocation;   // = -1;
    // uniforms locations
    GLint PVMmatrixLocation;  // = -1;
    GLint timeLocation;       // = -1;
    GLint texSamplerLocation; // = -1;
} bannerShaderProgram;

struct SkyboxShaderProgram {
    // identifier for the shader program
    GLuint program;                 // = 0;
    // vertex attributes locations
    GLint screenCoordLocation;      // = -1;
    // uniforms locations
    GLint inversePVmatrixLocation; // = -1;
    GLint skyboxSamplerLocation;    // = -1;
    //fog uniforms
    GLint fog;
} skyboxShaderProgram;
/*
 * Load mesh using assimp library
 * Vertex, normals and texture coordinates data are stored without interleaving |VVVVV...|NNNNN...|tttt
 * \param fileName [in] file to open/load
 * \param shader [in] vao will connect loaded data to shader
 * \param geometry
 */
bool loadSingleMesh(const std::string& fileName, ShaderProgram& shader, ObjectGeometry** geometry, bool normalize) {
    Assimp::Importer importer;

    // Unitize object in size (scale the model to fit into (-1..1)^3)
    if (normalize) {
        importer.SetPropertyInteger(AI_CONFIG_PP_PTV_NORMALIZE, 1);
    }

    // Load asset from the file - you can play with various processing steps
    const aiScene* scn = importer.ReadFile(fileName.c_str(), 0
        | aiProcess_Triangulate             // Triangulate polygons (if any).
        | aiProcess_PreTransformVertices    // Transforms scene hierarchy into one root with geometry-leafs only. For more see Doc.
        | aiProcess_GenSmoothNormals        // Calculate normals per vertex.
        | aiProcess_JoinIdenticalVertices);

    // abort if the loader fails
    if (scn == NULL) {
        std::cerr << "assimp error: " << importer.GetErrorString() << std::endl;
        *geometry = NULL;
        return false;
    }

    // some formats store whole scene (multiple meshes and materials, lights, cameras, ...) in one file, we cannot handle that in our simplified example
    if (scn->mNumMeshes != 1) {
        std::cerr << "this simplified loader can only process files with only one mesh" << std::endl;
        *geometry = NULL;
        return false;
    }

    // in this phase we know we have one mesh in our loaded scene, we can directly copy its data to OpenGL ...
    const aiMesh* mesh = scn->mMeshes[0];

    *geometry = new ObjectGeometry;

    // vertex buffer object, store all vertex positions and normals
    glGenBuffers(1, &((*geometry)->vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float) * mesh->mNumVertices, 0, GL_STATIC_DRAW); // allocate memory for vertices, normals, and texture coordinates
    // first store all vertices
    glBufferSubData(GL_ARRAY_BUFFER, 0, 3 * sizeof(float) * mesh->mNumVertices, mesh->mVertices);
    // then store all normals
    glBufferSubData(GL_ARRAY_BUFFER, 3 * sizeof(float) * mesh->mNumVertices, 3 * sizeof(float) * mesh->mNumVertices, mesh->mNormals);

    // just texture 0 for now
    float* textureCoords = new float[2 * mesh->mNumVertices];  // 2 floats per vertex
    float* currentTextureCoord = textureCoords;

    // copy texture coordinates
    aiVector3D vect;

    if (mesh->HasTextureCoords(0)) {
        // we use 2D textures with 2 coordinates and ignore the third coordinate
        for (unsigned int idx = 0; idx < mesh->mNumVertices; idx++) {
            vect = (mesh->mTextureCoords[0])[idx];
            *currentTextureCoord++ = vect.x;
            *currentTextureCoord++ = vect.y;
        }
    }

    // finally store all texture coordinates
    glBufferSubData(GL_ARRAY_BUFFER, 6 * sizeof(float) * mesh->mNumVertices, 2 * sizeof(float) * mesh->mNumVertices, textureCoords);

    // copy all mesh faces into one big array (assimp supports faces with ordinary number of vertices, we use only 3 -> triangles)
    unsigned int* indices = new unsigned int[mesh->mNumFaces * 3];
    for (unsigned int f = 0; f < mesh->mNumFaces; ++f) {
        indices[f * 3 + 0] = mesh->mFaces[f].mIndices[0];
        indices[f * 3 + 1] = mesh->mFaces[f].mIndices[1];
        indices[f * 3 + 2] = mesh->mFaces[f].mIndices[2];
    }

    // copy our temporary index array to OpenGL and free the array
    glGenBuffers(1, &((*geometry)->elementBufferObject));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned) * mesh->mNumFaces, indices, GL_STATIC_DRAW);

    delete[] indices;

    // copy the material info to ObjectGeometry structure
    const aiMaterial* mat = scn->mMaterials[mesh->mMaterialIndex];
    aiColor4D color;
    aiString name;
    aiReturn retValue = AI_SUCCESS;

    // Get returns: aiReturn_SUCCESS 0 | aiReturn_FAILURE -1 | aiReturn_OUTOFMEMORY -3
    mat->Get(AI_MATKEY_NAME, name); // may be "" after the input mesh processing. Must be aiString type!

    if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_DIFFUSE, &color)) != AI_SUCCESS)
        color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);

    (*geometry)->diffuse = glm::vec3(color.r, color.g, color.b);

    if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_AMBIENT, &color)) != AI_SUCCESS)
        color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
    (*geometry)->ambient = glm::vec3(color.r, color.g, color.b);

    if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_SPECULAR, &color)) != AI_SUCCESS)
        color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
    (*geometry)->specular = glm::vec3(color.r, color.g, color.b);

    ai_real shininess, strength;
    unsigned int max;	// changed: to unsigned

    max = 1;
    if ((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS, &shininess, &max)) != AI_SUCCESS)
        shininess = 1.0f;
    max = 1;
    if ((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS_STRENGTH, &strength, &max)) != AI_SUCCESS)
        strength = 1.0f;
    (*geometry)->shininess = shininess * strength;

    (*geometry)->texture = 0;

    // load texture image
    if (mat->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
        // get texture name 
        aiString path; // filename

        aiReturn texFound = mat->GetTexture(aiTextureType_DIFFUSE, 0, &path);
        std::string textureName = path.data;

        size_t found = fileName.find_last_of("/\\");
        // insert correct texture file path 
        if (found != std::string::npos) { // not found
          //subMesh_p->textureName.insert(0, "/");
            textureName.insert(0, fileName.substr(0, found + 1));
        }

        std::cout << "Loading texture file: " << textureName << std::endl;
        (*geometry)->texture = pgr::createTexture(textureName);
    }

    CHECK_GL_ERROR();

    glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
    glBindVertexArray((*geometry)->vertexArrayObject);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject); // bind our element array buffer (indices) to vao
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);

    glEnableVertexAttribArray(shader.locations.pos);
    glVertexAttribPointer(shader.locations.pos, 3, GL_FLOAT, GL_FALSE, 0, 0);

    //if (useLighting == true) {
        glEnableVertexAttribArray(shader.locations.normal);
        glVertexAttribPointer(shader.locations.normal, 3, GL_FLOAT, GL_FALSE, 0, (void*)(3 * sizeof(float) * mesh->mNumVertices));
   // }
    //else {
       // glDisableVertexAttribArray(shader.locations.color);
        // following line is problematic on AMD/ATI graphic cards
        // -> if you see black screen (no objects at all) than try to set color manually in vertex shader to see at least something
      //  glVertexAttrib3f(shader.locations.color, color.r, color.g, color.b);
   // }

    glEnableVertexAttribArray(shader.locations.texCoord);
    glVertexAttribPointer(shader.locations.texCoord, 2, GL_FLOAT, GL_FALSE, 0, (void*)(6 * sizeof(float) * mesh->mNumVertices));
    CHECK_GL_ERROR();

    glBindVertexArray(0);

    (*geometry)->numTriangles = mesh->mNumFaces;

    return true;
}

/*
 * Align (rotate and move) current coordinate system to given parameters.
 *
 * This function works similarly to \ref gluLookAt, however it is used for object transform
 * rather than view transform. The current coordinate system is moved so that origin is moved
 * to the \a position. Object's local front (-Z) direction is rotated to the \a front and
 * object's local up (+Y) direction is rotated so that angle between its local up direction and
 * \a up vector is minimum.
 *
 * \param[in]  position           Position of the origin.
 * \param[in]  front              Front direction.
 * \param[in]  up                 Up vector.
*/
glm::mat4 alignObject(const glm::vec3& position, const glm::vec3& front, const glm::vec3& up) {
    glm::vec3 z = -glm::normalize(front);

    if (!z.x && !z.y && !z.z)
        z = glm::vec3(0.0, 0.0, 1.0);

    glm::vec3 x = glm::normalize(glm::cross(up, z));

    if (!x.x && !x.y && !x.z)
        x = glm::vec3(1.0, 0.0, 0.0);

    glm::vec3 y = glm::cross(z, x);
    glm::mat4 matrix = glm::mat4(
        x.x, x.y, x.z, 0.0,
        y.x, y.y, y.z, 0.0,
        z.x, z.y, z.z, 0.0,
        position.x, position.y, position.z, 1.0
    );

    return matrix;
}
/**
 * \brief Set transfrom uniforms.
 * \param modelMatrix model matrix
 * \param viewMatrix view matrix
 * \param projectionMatrix projection matrix
 */
void setTransformUniforms(const glm::mat4& modelMatrix, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glm::mat4 PVM = projectionMatrix * viewMatrix * modelMatrix;
    glUniformMatrix4fv(shaderProgram.locations.PVMmatrix, 1, GL_FALSE, glm::value_ptr(PVM));
    glUniformMatrix4fv(shaderProgram.locations.Vmatrix, 1, GL_FALSE, glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(shaderProgram.locations.Mmatrix, 1, GL_FALSE, glm::value_ptr(modelMatrix));
    // just take 3x3 rotation part of the modelMatrix
    // we presume the last row contains 0,0,0,1
    const glm::mat4 modelRotationMatrix = glm::mat4(
        modelMatrix[0],
        modelMatrix[1],
        modelMatrix[2],
        glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)
    );
    glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelRotationMatrix));
    glUniformMatrix4fv(shaderProgram.locations.normalMatrix, 1, GL_FALSE, glm::value_ptr(normalMatrix));  // correct matrix for non-rigid transform
}
/**
 * \brief Set material uniforms.
 * \param ambient ambient component
 * \param diffuse diffuse component
 * \param specular specular component
 * \param shininess shininess
 * \param texture texture
 */
void setMaterialUniforms(const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular, float shininess, GLuint texture) {
    glUniform3fv(shaderProgram.locations.diffuse, 1, glm::value_ptr(diffuse)); 
    glUniform3fv(shaderProgram.locations.ambient, 1, glm::value_ptr(ambient));
    glUniform3fv(shaderProgram.locations.specular, 1, glm::value_ptr(specular));
    glUniform1f(shaderProgram.locations.shininess, shininess);

    if (texture != 0) {
        glUniform1i(shaderProgram.locations.useTexture, 1);  // do texture sampling
        glUniform1i(shaderProgram.locations.texSampler, 0);  // texturing unit 0 -> samplerID   [for the GPU linker]
        glActiveTexture(GL_TEXTURE0 + 0);                  // texturing unit 0 -> to be bound [for OpenGL BindTexture]
        glBindTexture(GL_TEXTURE_2D, texture);
    }
    else {
        glUniform1i(shaderProgram.locations.useTexture, 0);  // do not sample the texture
    }
}
/**
 * \brief Initialize all shader programs including skybox, fire and banner shaders
 */
void initializeShaderPrograms(void) {
    std::vector<GLuint> shaderList;
        // load and compile shader for lighting (lights & materials)

        // push vertex shader and fragment shader
        shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "mainVertexShader.vert"));
        shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "mainFragmentShader.frag"));

        // create the shader program with two shaders
        shaderProgram.program = pgr::createProgram(shaderList);

        // get vertex attributes locations, if the shader does not have this uniform -> return -1
        shaderProgram.locations.pos = glGetAttribLocation(shaderProgram.program, "position");
        shaderProgram.locations.normal = glGetAttribLocation(shaderProgram.program, "normal");
        shaderProgram.locations.texCoord = glGetAttribLocation(shaderProgram.program, "texCoord");
        // get uniforms locations
        shaderProgram.locations.PVMmatrix = glGetUniformLocation(shaderProgram.program, "PVMmatrix");
        shaderProgram.locations.Vmatrix = glGetUniformLocation(shaderProgram.program, "Vmatrix");
        shaderProgram.locations.Mmatrix = glGetUniformLocation(shaderProgram.program, "Mmatrix");
        shaderProgram.locations.normalMatrix = glGetUniformLocation(shaderProgram.program, "normalMatrix");
        shaderProgram.locations.time = glGetUniformLocation(shaderProgram.program, "time");
        // material
        shaderProgram.locations.ambient = glGetUniformLocation(shaderProgram.program, "material.ambient");
        shaderProgram.locations.diffuse = glGetUniformLocation(shaderProgram.program, "material.diffuse");
        shaderProgram.locations.specular = glGetUniformLocation(shaderProgram.program, "material.specular");
        shaderProgram.locations.shininess = glGetUniformLocation(shaderProgram.program, "material.shininess");
        // texture
        shaderProgram.locations.texSampler = glGetUniformLocation(shaderProgram.program, "texSampler");
        shaderProgram.locations.useTexture = glGetUniformLocation(shaderProgram.program, "material.useTexture");
        //lamp 
        shaderProgram.locations.lamp = glGetUniformLocation(shaderProgram.program, "lampLightVisible");
        shaderProgram.locations.lampPosition = glGetUniformLocation(shaderProgram.program, "lampPosition");
        //fire
        shaderProgram.locations.fire = glGetUniformLocation(shaderProgram.program, "fireVisible");
        shaderProgram.locations.firePosition = glGetUniformLocation(shaderProgram.program, "firePosition");
        //flashlight
        shaderProgram.locations.flashlight = glGetUniformLocation(shaderProgram.program, "flashlightVisible");
        shaderProgram.locations.flashlightPosition = glGetUniformLocation(shaderProgram.program, "flashlightPosition");
        shaderProgram.locations.flashlightDirection = glGetUniformLocation(shaderProgram.program, "flashlightDirection");
        shaderProgram.locations.flashlightIntensity = glGetUniformLocation(shaderProgram.program, "flashlightIntensity");
        //fog
        shaderProgram.locations.fog = glGetUniformLocation(shaderProgram.program, "fogVisible");
    // load and compile shader for skybox (cube map)
    shaderList.clear();

    // push vertex shader and fragment shader
    shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "skyboxVertexShader.vert"));
    shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "skyboxFragmentShader.frag"));

    // create the program with two shaders
    skyboxShaderProgram.program = pgr::createProgram(shaderList);

    // handles to vertex attributes locations
    skyboxShaderProgram.screenCoordLocation = glGetAttribLocation(skyboxShaderProgram.program, "screenCoord");
    // get uniforms locations
    skyboxShaderProgram.skyboxSamplerLocation = glGetUniformLocation(skyboxShaderProgram.program, "skyboxSampler");
    skyboxShaderProgram.inversePVmatrixLocation = glGetUniformLocation(skyboxShaderProgram.program, "inversePVmatrix");
    //fog uniform
    skyboxShaderProgram.fog = glGetUniformLocation(skyboxShaderProgram.program, "fogVisible");
    // load and compile shader for banner
    shaderList.clear();

    // push vertex shader and fragment shader
    shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "bannerVertexShader.vert"));
    shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "bannerFragmentShader.frag"));

    // Create the program with two shaders
    bannerShaderProgram.program = pgr::createProgram(shaderList);

    // get position and color attributes locations
    bannerShaderProgram.posLocation = glGetAttribLocation(bannerShaderProgram.program, "position");
    bannerShaderProgram.texCoordLocation = glGetAttribLocation(bannerShaderProgram.program, "texCoord");

    // get uniforms locations
    bannerShaderProgram.PVMmatrixLocation = glGetUniformLocation(bannerShaderProgram.program, "PVMmatrix");
    bannerShaderProgram.timeLocation = glGetUniformLocation(bannerShaderProgram.program, "time");
    bannerShaderProgram.texSamplerLocation = glGetUniformLocation(bannerShaderProgram.program, "texSampler");

    // load and compile shader for fire
    shaderList.clear();

    // push vertex shader and fragment shader
    shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "fireVertexShader.vert"));
    shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "fireFragmentShader.frag"));

    // Create the program with two shaders
    fireShaderProgram.program = pgr::createProgram(shaderList);

    // get position and texture coordinates attributes locations
    fireShaderProgram.posLocation = glGetAttribLocation(fireShaderProgram.program, "position");
    fireShaderProgram.texCoordLocation = glGetAttribLocation(fireShaderProgram.program, "texCoord");

    // get uniforms locations
    fireShaderProgram.PVMmatrixLocation = glGetUniformLocation(fireShaderProgram.program, "PVMmatrix");
    fireShaderProgram.VmatrixLocation = glGetUniformLocation(fireShaderProgram.program, "Vmatrix");
    fireShaderProgram.timeLocation = glGetUniformLocation(fireShaderProgram.program, "time");
    fireShaderProgram.texSamplerLocation = glGetUniformLocation(fireShaderProgram.program, "texSampler");
    fireShaderProgram.frameDurationLocation = glGetUniformLocation(fireShaderProgram.program, "frameDuration");
}
/**
 * \brief Clean up all shader programs including skybox, fire and banner shaders
 */
void cleanupShaderPrograms(void) {
    pgr::deleteProgramAndShaders(shaderProgram.program);
    pgr::deleteProgramAndShaders(skyboxShaderProgram.program);
    pgr::deleteProgramAndShaders(fireShaderProgram.program);
    pgr::deleteProgramAndShaders(bannerShaderProgram.program);
}

//-----------------BASE OBJECT---------------
/**
 * \brief Base object inicialization.
 * \param GEOMETRY_FILE path to file with geometry
 * \param TEXTURE_FILE path to file with texture
 */
BaseObject::BaseObject(const char* GEOMETRY_FILE, const char* TEXTURE_FILE)
{
    if (loadSingleMesh(GEOMETRY_FILE, shaderProgram, &geometry, false) != true) {
        std::cerr << "initializeModels(): Base model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    //init material and texture
    geometry->ambient = glm::vec3(1.0f, 1.0f, 1.0f);
    geometry->diffuse = glm::vec3(0.6f, 0.7f, 0.5f);
    geometry->specular = glm::vec3(0.01f, 0.02f, 0.02f);
    geometry->shininess = 0.5f;
    geometry->texture = pgr::createTexture(TEXTURE_FILE);
}
/**
 * \brief Draw base object including right position, texture and materials.
 * \param viewMatrix view matrix
 * \param projectionMatrix projection matrix
 */
void BaseObject::draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix)
{
    glUseProgram(shaderProgram.program);

    // align base object coordinate system to match its position and direction
    glm::mat4 modelMatrix = alignObject(position, direction, glm::vec3(0.0f, 1.0f, 1.0f));
    modelMatrix = glm::scale(modelMatrix, glm::vec3(size));
    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    setMaterialUniforms(
        geometry->ambient,
        geometry->diffuse,
        geometry->specular,
        geometry->shininess,
        geometry->texture
    );

    glBindVertexArray(geometry->vertexArrayObject);
    glDrawElements(GL_TRIANGLES, geometry->numTriangles * 3, GL_UNSIGNED_INT, 0);

    CHECK_GL_ERROR();

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}
//-----------------BASE OBJECT---------------END

//------------CORGI--------------
/**
 * \brief Corgi object inicialization.
 * \param TEXTURE_FILE path to file with texture
 */
CorgiObject::CorgiObject(const char* TEXTURE_FILE)
{
    geometry = new ObjectGeometry;
    //vao
    glGenVertexArrays(1, &(geometry->vertexArrayObject));
    glBindVertexArray(geometry->vertexArrayObject);
    //vbo
    glGenBuffers(1, &(geometry->vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, geometry->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, sizeof(corgiVertices), corgiVertices, GL_STATIC_DRAW);

    // ebo, copy our temporary index array to opengl and free the array
    glGenBuffers(1, &(geometry->elementBufferObject));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry->elementBufferObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned int) * corgiNTriangles, corgiTriangles, GL_STATIC_DRAW);

    glEnableVertexAttribArray(shaderProgram.locations.pos);
    glVertexAttribPointer(shaderProgram.locations.pos, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), 0);

    glEnableVertexAttribArray(shaderProgram.locations.normal);
    glVertexAttribPointer(shaderProgram.locations.normal, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));

    glEnableVertexAttribArray(shaderProgram.locations.texCoord);
    glVertexAttribPointer(shaderProgram.locations.texCoord, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));

    //init material and texture
    geometry->ambient = glm::vec3(1.0f, 1.0f, 1.0f);
    geometry->diffuse = glm::vec3(0.6f, 0.7f, 0.5f);
    geometry->specular = glm::vec3(0.01f, 0.02f, 0.02f);
    geometry->shininess = 0.5f;
    geometry->texture = pgr::createTexture(TEXTURE_FILE);

    glBindVertexArray(0);

    geometry->numTriangles = corgiNTriangles;
}
/**
 * \brief Draw corgi object including right position, texture and materials.
 * \param viewMatrix view matrix
 * \param projectionMatrix projection matrix
 */
void CorgiObject::draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix)
{
    glUseProgram(shaderProgram.program);

    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), position);
    modelMatrix = glm::scale(modelMatrix, glm::vec3(size));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(90.0f), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(-110.0f), glm::vec3(0, 1, 0));
    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    setMaterialUniforms(
        geometry->ambient,
        geometry->diffuse,
        geometry->specular,
        geometry->shininess,
        geometry->texture
    );

    glBindVertexArray(geometry->vertexArrayObject);
    glDrawElements(GL_TRIANGLES, geometry->numTriangles * 3, GL_UNSIGNED_INT, 0);

    CHECK_GL_ERROR();

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}
//--------------CORGI----------END



//----------BANNER-------
/**
 * \brief Banner object inicialization.
 * \param TEXTURE_FILE path to file with texture
 */
BannerObject::BannerObject(const char* TEXTURE_FILE)
{
    geometry = new ObjectGeometry;

    geometry->texture = pgr::createTexture(TEXTURE_FILE);
    glBindTexture(GL_TEXTURE_2D, geometry->texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glGenVertexArrays(1, &(geometry->vertexArrayObject));
    glBindVertexArray(geometry->vertexArrayObject);

    glGenBuffers(1, &(geometry->vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, geometry->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bannerVertexData), bannerVertexData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(bannerShaderProgram.posLocation);
    glEnableVertexAttribArray(bannerShaderProgram.texCoordLocation);
    // vertices of triangles - start at the beginning of the interlaced array
    glVertexAttribPointer(bannerShaderProgram.posLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
    // texture coordinates of each vertices are stored just after its position
    glVertexAttribPointer(bannerShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

    glBindVertexArray(0);

    geometry->numTriangles = bannerNumQuadVertices;
}
/**
 * \brief Draw banner object including right position, texture and materials.
 * \param viewMatrix view matrix
 * \param projectionMatrix projection matrix
 */
void BannerObject::draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_DEPTH_TEST);

    glUseProgram(bannerShaderProgram.program);

    glm::mat4 matrix = glm::translate(glm::mat4(1.0f), position);
    matrix = glm::scale(matrix, glm::vec3(size));

    glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * matrix;
    glUniformMatrix4fv(bannerShaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));        // model-view-projection
    glUniform1f(bannerShaderProgram.timeLocation, currentTime - startTime);
    glUniform1i(bannerShaderProgram.texSamplerLocation, 0);

    glBindTexture(GL_TEXTURE_2D, geometry->texture);
    glBindVertexArray(geometry->vertexArrayObject);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, geometry->numTriangles);

    CHECK_GL_ERROR();

    glBindVertexArray(0);
    glUseProgram(0);

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);

    return;
}
//----------BANNER-------END


//-----------FIRE ANIMATION---------
/**
 * \brief Fire object inicialization.
 * \param TEXTURE_FILE path to file with texture
 */
FireObject::FireObject(const char* TEXTURE_FILE)
{
    geometry = new ObjectGeometry;

    //init material and texture
    geometry->ambient = glm::vec3(1.0f, 1.0f, 1.0f);
    geometry->diffuse = glm::vec3(0.6f, 0.7f, 0.5f);
    geometry->specular = glm::vec3(0.01f, 0.02f, 0.02f);
    geometry->shininess = 0.5f;
    geometry->texture = pgr::createTexture(TEXTURE_FILE);

    glGenVertexArrays(1, &(geometry->vertexArrayObject));
    glBindVertexArray(geometry->vertexArrayObject);

    glGenBuffers(1, &(geometry->vertexBufferObject)); 
    glBindBuffer(GL_ARRAY_BUFFER, geometry->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, sizeof(fireVertexData), fireVertexData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(fireShaderProgram.posLocation);
    // vertices of triangles - start at the beginning of the array (interlaced array)
    glVertexAttribPointer(fireShaderProgram.posLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);

    glEnableVertexAttribArray(fireShaderProgram.texCoordLocation);
    // texture coordinates are placed just after the position of each vertex (interlaced array)
    glVertexAttribPointer(fireShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

    glBindVertexArray(0);

    geometry->numTriangles = fireNumQuadVertices;
}
/**
 * \brief Draw base object including right position, texture and materials.
 * \param viewMatrix view matrix
 * \param projectionMatrix projection matrix
 */
void FireObject::draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);

    glUseProgram(fireShaderProgram.program);

    // just take rotation part of the view transform
    glm::mat4 fireRotationMatrix = glm::mat4(
        viewMatrix[0],
        viewMatrix[1],
        viewMatrix[2],
        glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)
    );
    // inverse view rotation
    fireRotationMatrix = glm::transpose(fireRotationMatrix);

    glm::mat4 matrix = glm::translate(glm::mat4(1.0f), position);
    matrix = glm::scale(matrix, glm::vec3(size));
    matrix = matrix * fireRotationMatrix; // make fire to face the camera

    glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * matrix;
    glUniformMatrix4fv(fireShaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));  // model-view-projection
    glUniformMatrix4fv(fireShaderProgram.VmatrixLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));   // view
    glUniform1f(fireShaderProgram.timeLocation, currentTime - startTime);
    glUniform1i(fireShaderProgram.texSamplerLocation, 0);
    glUniform1f(fireShaderProgram.frameDurationLocation, frameDuration);


    glBindVertexArray(geometry->vertexArrayObject);
    glBindTexture(GL_TEXTURE_2D, geometry->texture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, geometry->numTriangles);

    glBindVertexArray(0);
    glUseProgram(0);

    glDisable(GL_BLEND);

    glUseProgram(fireShaderProgram.program);
}
//---------------FIRE---------END

//------------MOVEABLE DUCK--------
/**
 * \brief Duck object inicialization.
 * \param GEOMETRY_FILE path to file with geometry
 * \param TEXTURE_FILE path to file with texture
 */
DuckObject::DuckObject(const char* GEOMETRY_FILE, const char* TEXTURE_FILE)
{
    if (loadSingleMesh(GEOMETRY_FILE, shaderProgram, &geometry, false) != true) {
        std::cerr << "initializeModels(): Base model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    //init material and texture
    geometry->ambient = glm::vec3(1.0f, 1.0f, 1.0f);
    geometry->diffuse = glm::vec3(0.6f, 0.7f, 0.5f);
    geometry->specular = glm::vec3(0.01f, 0.02f, 0.02f);
    geometry->shininess = 0.5f;
    geometry->texture = pgr::createTexture(TEXTURE_FILE);
}
/**
 * \brief Draw duck object including right position, texture and materials.
 * \param viewMatrix view matrix
 * \param projectionMatrix projection matrix
 */
void DuckObject::draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix)
{
    glUseProgram(shaderProgram.program);
 
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), position);
    modelMatrix = glm::scale(modelMatrix, glm::vec3(size));
    modelMatrix = glm::rotate(modelMatrix, viewAngle, glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, 3.14f/2.0f, glm::vec3(1, 0, 0));

    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);

    setMaterialUniforms(
        geometry->ambient,
        geometry->diffuse,
        geometry->specular,
        geometry->shininess,
        geometry->texture
    );

    // draw geometry
    glBindVertexArray(geometry->vertexArrayObject);
    glDrawElements(GL_TRIANGLES, geometry->numTriangles * 3, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}

//------------MOVEABLE DUCK--------END--------------

//------------SKYBOX--------------------
/**
 * \brief Skybox object inicialization.
 * \param SKYBOX_CUBE_TEXTURE_FILE_PREFIX path to file with geometry
 */
SkyboxObject::SkyboxObject(const char* SKYBOX_CUBE_TEXTURE_FILE_PREFIX)
{
    geometry = new ObjectGeometry;

    // 2D coordinates of 2 triangles covering the whole screen (NDC), draw using triangle strip
    static const float screenCoords[] = {
      -1.0f, -1.0f,
       1.0f, -1.0f,
      -1.0f,  1.0f,
       1.0f,  1.0f
    };

    glGenVertexArrays(1, &(geometry->vertexArrayObject));
    glBindVertexArray(geometry->vertexArrayObject);

    // buffer for far plane rendering
    glGenBuffers(1, &(geometry->vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, geometry->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, sizeof(screenCoords), screenCoords, GL_STATIC_DRAW);

    //glUseProgram(farplaneShaderProgram);

    glEnableVertexAttribArray(skyboxShaderProgram.screenCoordLocation);
    glVertexAttribPointer(skyboxShaderProgram.screenCoordLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);
    glUseProgram(0);
    CHECK_GL_ERROR();

    geometry->numTriangles = 2;

    glActiveTexture(GL_TEXTURE0);

    glGenTextures(1, &(geometry->texture));
    glBindTexture(GL_TEXTURE_CUBE_MAP, geometry->texture);

    const char* suffixes[] = { "posx", "negx", "posy", "negy", "posz", "negz" };
    GLuint targets[] = {
      GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
      GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
      GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
    };

    for (int i = 0; i < 6; i++) {
        std::string texName = std::string(SKYBOX_CUBE_TEXTURE_FILE_PREFIX) + "_" + suffixes[i] + ".jpg";
        std::cout << "Loading cube map texture: " << texName << std::endl;
        if (!pgr::loadTexImage2D(texName, targets[i])) {
            pgr::dieWithError("Skybox cube map loading failed!");
        }
    }

    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    // unbind the texture (just in case someone will mess up with texture calls later)
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    CHECK_GL_ERROR();
}
/**
 * \brief Draw duck object including right position, texture and materials.
 * \param viewMatrix view matrix
 * \param projectionMatrix projection matrix
 * \param fogVisible boolean if fog visible = true, else = false
 */
void SkyboxObject::draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, bool fogVisible)
{
    glUseProgram(skyboxShaderProgram.program);

    // compose transformations
    glm::mat4 matrix = projectionMatrix * viewMatrix;

    // create view rotation matrix by using view matrix with cleared translation
    glm::mat4 viewRotation = viewMatrix;
    viewRotation[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

    // vertex shader will translate screen space coordinates (NDC) using inverse PV matrix
    glm::mat4 inversePVmatrix = glm::inverse(projectionMatrix * viewRotation);

    glUniformMatrix4fv(skyboxShaderProgram.inversePVmatrixLocation, 1, GL_FALSE, glm::value_ptr(inversePVmatrix));
    glUniform1i(skyboxShaderProgram.skyboxSamplerLocation, 0);

    glUniform1i(skyboxShaderProgram.fog, fogVisible);

    // draw "skybox" rendering 2 triangles covering the far plane
    glBindVertexArray(geometry->vertexArrayObject);
    glBindTexture(GL_TEXTURE_CUBE_MAP, geometry->texture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, geometry->numTriangles + 2);

    glBindVertexArray(0);
    glUseProgram(0);
}
//---------------------SKYBOX---------END
